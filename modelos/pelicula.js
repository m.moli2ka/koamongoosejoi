const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const peliculaSchema = new Schema({
    Title: String,
    Year: String,
    Rated: String,
    Released: String,
    Genre: String,
    Director: String,
    Actors: String,
    Plot: String,
    Ratings: Array
});

const Pelicula = mongoose.model('peliculas', peliculaSchema);


module.exports = Pelicula;