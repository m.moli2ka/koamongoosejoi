const Joi = require('joi');


const updatePlot = Joi.object({
    newPlotString: Joi.string().required().min(3),
    plotString: Joi.string().required(),
    id: Joi.required(),
    plot: Joi.required()
})

module.exports = {
    updatePlot
}