// se utilizo npm i koa koa-ejs koa-bodyparser koa-static koa-router
// Se utilizo nodemon para facilitar la programacion
// Posteriormente se agrego la base de datos (mongoose) y un controlador para poder utilizar los datos del host con .env
const Koa = require('koa');
const static = require('koa-static');
const KoaRouter = require('koa-router');
const path = require('path');
const render = require('koa-ejs');
const bodyParser = require('koa-bodyparser');
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;
const { updatePlot } = require('./helpers/validation_schema');
require('dotenv').config();



const app = new Koa();

// Se crea un objeto route del paquete koa-router
const route = new KoaRouter();

// Conectarse a la BD

const db = mongoose.connection;
const host = process.env.host;
const dbupdate = {
    useNewUrlParser:true,
    useUnifiedTopology: true,
    useFindAndModify: false};
mongoose.connect(host,dbupdate);

db.on('error',(err) => console.log('Error, BD no esta conectada', err));
db.on('connected', () => console.log('Conectado a mongo'));
db.on('disconnected', () => console.log('Desconectado de mongo'));
db.on('open', () => console.log('Conexion Realizada'));




// Schema de modelos

const Pelicula = require('./modelos/pelicula.js');
const { isValidObjectId } = require('mongoose');
const { update } = require('./modelos/pelicula.js');

// MiddleWares: Utilizacion de los paquetes

// Ejs Render de imagen

render(app, {
    root: path.join(__dirname,'vistas'),
    layout: 'layout',
    viewExt: 'html',
    cache: false,
    debug:false
});

// BodyParser

app.use(bodyParser());

// Rutas

app.use(route.routes());

// Static :  Carpeta estatica para acceder a ella con el framework koa
app.use(static('./public'));


//  Rutas con metodos
// GET POST PUT DEL PATCH

// Ejemplo funcional
route.get('/test',(ctx,next) => ctx.body = "Hola Test");

// Buscar Peliculas
// 1.- El valor a buscar debe ir en la url ( /:id )
// 2.- Header  con el año de la película
// 3.- 1 registro por pelicula
// 4.- Mostrar todos

route.get('/show/:id', async (ctx, next) => {
    const query = Pelicula.findById(ctx.params.id);
    query instanceof mongoose.Query;
    const docs = await query;
    // console.log(docs);
    await ctx.render('show',{
        title:'Pelicula ',
        pelicula : docs
    });
});



// Obtener todas las películas
// 1.- Se deben devolver todas las peliculas que se han guardado en la bd
// 2.- Si hay mas de 5 peliculas guardadas en bd se debe paginar de 5 en 5
// 3.- el nuemro de pagina debe ir por header.



route.get('/mostrarPeliculas', async (ctx) => {
    const query = Pelicula.find();
    query instanceof mongoose.Query;
    const docs = await query;
    await ctx.render('index',{
        title:'Index',
        peliculas : docs        
    });

});
/*

o	Método POST que reciba en el BODY un object como: P.E: {movie: star wars, find: jedi, replace: CLM Dev }
o	Buscar dentro de la BD y obtener el campo PLOT del registro
o	Al string del plot obtenido buscar la palabra enviada en el Body (find) y reemplazar todas sus ocurrencias por el campo enviado en el body (replace)
o	Devolver el string con las modificaciones del punto anterior


*/



route.get('/edit/:id' ,async (ctx) =>{ 
    
    const query = Pelicula.findById(ObjectId(ctx.params.id));
    query instanceof mongoose.Query;
    const docs = await query;
    // console.log(docs);
    await ctx.render('edit',{
        title:'Pelicula ',
        pelicula : docs
    });

});

route.post('/edit', async (ctx) => {
    const body = ctx.request.body;

    // Validacion de campos con Joi
    
    const result = await updatePlot.validateAsync(body);
    
    // Si plotString existe en Plot
    if(result){
        const id = result.id;
        const newPlotString = result.newPlotString;
        const PlotString = result.plotString;
        var plot = result.plot;
        if(plot.includes(PlotString)){
            const query = Pelicula.findById(ObjectId(id));
            
            query instanceof mongoose.Query;
            const docs = await query;
            console.log(result.newPlotString)
            plot = plot.replace(PlotString, newPlotString);
            Pelicula.updateOne({_id: ObjectId(id)},{"Plot": plot},(error,result) => {
                console.log(result)
            })
        }else{
            
            console.log("No existe esa palabra en la trama");
        } 


    }else {
        console.log("Validation Error")
    }
    
    
    const query = Pelicula.findById(ObjectId(result.id));
    query instanceof mongoose.Query;
    const docs = await query;
    await ctx.render('show',{
        title:'Pelicula ',
        pelicula : docs
    });

});



// Escuchando en el puerto 3000
app.listen(3000,()=> console.log('Escuchando el puerto 3000'));